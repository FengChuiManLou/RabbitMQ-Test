# RabbitMQ-Test

#### 项目介绍
RabbitMQ学习实践案例，这个案例分为两个部分，消息生产者和消息消费者。模拟用户注册场景，生产者将用户手机号发送到MQ，消费者监听MQ队列，获取用户手机号发送短信。

具体实现功能如下：

- 使用RabbitMQ客户端消息confirm和redis，确保消息正确发送到MQ，不会产生丢失；
- 结合消息过期（TTL）和死信队列（DLX）实现消费异常的延迟重试；
- 消息处理失败达到最大重试次数之后，将其发送到失败队列，等待人工处理。



具体细节见博客：https://blog.csdn.net/qq_36300264/article/details/82917038