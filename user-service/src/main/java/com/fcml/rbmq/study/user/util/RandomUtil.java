package com.fcml.rbmq.study.user.util;

import java.util.UUID;

public class RandomUtil {

    public static String getRandomUUID(int length) {
        return UUID.randomUUID().toString().substring(0, length);
    }
}
