package com.fcml.rbmq.study.sms.service;

import com.fcml.rbmq.study.sms.model.User;

public interface SmsService {
    void sendSms(User user);
}
