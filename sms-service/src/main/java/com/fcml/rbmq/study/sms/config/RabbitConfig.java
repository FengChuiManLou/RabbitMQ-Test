package com.fcml.rbmq.study.sms.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
public class RabbitConfig {
    /**
     * 声明一个交换机
     * @return
     */
    @Bean
    public TopicExchange smsCaptchaExchange() {
        return new TopicExchange("sms_captcha", true, false);
    }

    /**
     * 正常的消费队列
     * @return
     */
    @Bean
    public Queue smsCaptchaQueue() {
        return new Queue("sms@captcha", true, false, false);
    }
    /**
     * 延时重试队列
     */
    @Bean
    public Queue smsCaptchaRetryQueue() {
        Map<String, Object> arguments = new HashMap<>();
        arguments.put("x-message-ttl", 10 * 1000);
        arguments.put("x-dead-letter-exchange", "sms_captcha");
        arguments.put("x-dead-letter-routing-key", "sms.captcha");
        return new Queue("sms@captcha@retry", true, false, false, arguments);
    }
    /**
     * 处理失败后存放消息的队列
     * @return
     */
    @Bean
    public Queue smsCaptchaFailedQueue() {
        return new Queue("sms@captcha@failed", true, false, false);
    }



    /**
     * 将队列与交换机绑定
     * @return
     */
    @Bean
    public Binding smsCaptchaBinding() {
        return BindingBuilder.bind(smsCaptchaQueue()).to(smsCaptchaExchange()).with("sms.captcha");
    }
    @Bean
    public Binding smsCaptchaRetryBinding() {
        return BindingBuilder.bind(smsCaptchaRetryQueue()).to(smsCaptchaExchange()).with("sms.captcha.retry");
    }
    @Bean
    public Binding smsCaptchaFailedBinding() {
        return BindingBuilder.bind(smsCaptchaFailedQueue()).to(smsCaptchaExchange()).with("sms.captcha.failed");
    }
}
