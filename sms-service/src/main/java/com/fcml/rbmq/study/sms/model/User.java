package com.fcml.rbmq.study.sms.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Accessors(chain = true)
public class User implements Serializable {
    private static final long serialVersionUID = 8140146492499804675L;

    private Integer id;

    private String username;

    private String phoneNum;
}