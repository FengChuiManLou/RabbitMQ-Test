package com.fcml.rbmq.study.sms.service.impl;

import com.fcml.rbmq.study.sms.model.User;
import com.fcml.rbmq.study.sms.service.SmsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SmsServiceImpl implements SmsService {
    @Override
    public void sendSms(User user) {
        //System.out.println(1/0);
        log.info("Sms send to user: {} successfully", user.getId());
    }
}
